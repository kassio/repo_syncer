# frozen_string_literal: true

require_relative 'repos_syncer/config'
require_relative 'repos_syncer/repository'
require_relative 'repos_syncer/job'
require_relative 'repos_syncer/screen_updater'
require_relative 'repos_syncer/status_formatter'

module ReposSyncer
  class Syncer
    def initialize
      @config = Config.parse
      @formatter = StatusFormatter.new(colors: config.settings[:colors])
    end

    def sync
      benchmark do
        jobs = config.repositories.map do |name, attributes|
          Job.new(Repository.new(name, attributes), formatter).tap(&:start)
        end.sort_by(&:name)

        await(jobs)

        log(jobs)
      end
    end

    private

    attr_reader :config, :formatter

    def await(jobs)
      until jobs.all?(&:complete?)
        ScreenUpdater.autoclean do
          log(jobs)
          sleep 0.2
        end
      end
    end

    def benchmark
      start_mark = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      yield
      elapsed = Process.clock_gettime(Process::CLOCK_MONOTONIC) - start_mark

      puts "\n󰥔 #{elapsed.ceil(2)}s"
    end

    def log(jobs)
      puts jobs.map(&:status).join("\n")
    end
  end

  def self.sync! = Syncer.new.sync
end
