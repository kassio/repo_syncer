# frozen_string_literal: true

require 'tty/cursor'

module ReposSyncer
  class ScreenUpdater
    class << self
      def cursor
        @cursor ||= TTY::Cursor
      end

      def autoclean
        print cursor.save
        yield
        print cursor.restore
        print cursor.clear_screen_down
      end
    end
  end
end
