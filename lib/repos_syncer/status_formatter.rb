# frozen_string_literal: true

require 'pastel'

module ReposSyncer
  class StatusFormatter
    COLORS = {
      pending: :yellow,
      updating: :blue,
      failed: :red,
      completed: :green
    }.freeze

    def initialize(colors: nil)
      colors = true if colors.nil?

      @icons = {
        pending: "",
        updating: "",
        failed: "",
        completed: ""
      }

      if colors
        colorizer = Pastel.new
        COLORS.each do |key, color|
          @icons[key] = colorizer.decorate(@icons[key], color)
        end
      end
    end

    def format(data, indent: 0)
      return if data.nil?

      msg = "#{' ' * indent}#{icons[data[:status]]} #{data[:name]}"
      msg = "#{msg}: #{data[:message]}" unless data[:message].strip.empty?
      msg
    end

    def format_branches(data)
      return if data.nil?

      data.sort_by { |b| b[:name] }.map do |value|
        format(value, indent: 2)
      end
    end

    private

    attr_reader :icons
  end
end
