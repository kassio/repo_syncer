# frozen_string_literal: true

require_relative 'git'

module ReposSyncer
  class Repository
    INITIAL_STATUS = { name: "", status: :pending, message: "" }.freeze

    attr_reader :name,
      :local,
      :remote,
      :after_sync,
      :status

    def initialize(name, attributes)
      @name = name.to_s
      @local = attributes.fetch(:local).gsub('$HOME', Dir.home).gsub('${HOME}', Dir.home)
      @remote = attributes.fetch(:remote)
      @after_sync = attributes.fetch(:afterSync, nil)
      @fetch_all = attributes.fetch(:fetchAll, true)

      @status = INITIAL_STATUS.merge(name: name, status: :updating, branches: {})
      @status[:after_sync] = INITIAL_STATUS.merge(name: "after sync", message: after_sync) unless after_sync.nil?
    end

    def fetch_all? = @fetch_all

    def clone_or_update
      flow(status) do |state|
        if Dir.exist?(local)
          state.merge!(message: "update repository branches")
          update(state)
        else
          state.merge!(message: "clone repository")
          git.clone
        end

        state.merge!(message: "")
        call_after_sync
      end
    end

    def branches
      @branches ||= git.branches
    end

    private

    def update(state)
      initialize_branches_status
      state.merge!(message: "fetching remotes")

      git.fetch

      state.merge!(message: "updating branches")
      branches.each { |branch| update_branch(branch) }
    end

    def update_branch(branch)
      flow(status[:branches][branch]) do |state|
        remote = git.branch_remote(branch)
        state.merge!(message: "rebasing")
        git.rebase(remote, branch)
        state.merge!(message: "")
      end
    end

    def flow(status)
      status.merge!(status: :updating)
      yield(status)
      status.merge!(status: :completed)
    rescue StandardError => e
      status.merge!(status: :failed, message: e.message.join)
    end

    def initialize_branches_status
      branches.each do |branch|
        status[:branches][branch] = INITIAL_STATUS.merge(name: branch)
      end
    end

    def call_after_sync
      return if after_sync.nil?

      flow(status[:after_sync]) do
        git.repo_exec(*after_sync)
      end
    end

    def git
      @git ||= Git.new(self)
    end
  end
end
