# frozen_string_literal: true

require 'open3'

module ReposSyncer
  class Git
    class GitError < StandardError
      attr_reader :message, :status

      def initialize(message, status)
        @message = message
        @status = status
      end
    end

    def initialize(repo)
      @repo = repo
    end

    def clone
      git("clone #{repo.remote} #{repo.local}")
    end

    def fetch
      if repo.fetch_all?
        git("fetch --all", dir: repo.local)
      else
        git("fetch", dir: repo.local)
      end
    end

    def branches
      git("for-each-ref --format='%(refname:short)' refs/heads", dir: repo.local)
    end

    def branch_remote(branch)
      git("config --get branch.#{branch}.remote", dir: repo.local).first
    end

    def rebase(remote, branch)
      git("rebase #{remote}/#{branch} #{branch}", dir: repo.local)
    end

    def repo_exec(args)
      exec(args, dir: repo.local)
    end

    private

    attr_reader :repo

    def git(args, dir: nil)
      command = "git #{args}"
      output, status = exec(command, dir: dir)

      return output.split("\n").compact if status.success?

      raise GitError.new(output.split("\n").compact, status)
    end

    def exec(args, dir: nil)
      if dir.nil?
        Open3.capture2e(args)
      else
        Open3.capture2e(args, chdir: dir)
      end
    end
  end
end
