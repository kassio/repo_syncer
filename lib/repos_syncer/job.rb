# frozen_string_literal: true

require 'concurrent-ruby'

module ReposSyncer
  class Job
    class AsyncJob
      include Concurrent::Async

      def initialize(repo)
        @repo = repo
      end

      def start
        @repo.clone_or_update
      end
    end
    private_constant :AsyncJob

    def initialize(repo, formatter)
      @repo = repo
      @formatter = formatter
      @worker = AsyncJob.new(repo)
    end

    def name = repo.name
    def complete? = work.complete?

    def start
      @work = worker.async.start
    end

    def status
      [
        repo_status,
        branches_status,
        after_sync_status
      ].compact.join("\n").chomp
    end

    private

    attr_reader :repo, :worker, :work, :formatter

    def repo_status = formatter.format(repo.status)
    def branches_status = formatter.format_branches(repo.status[:branches].values)
    def after_sync_status = formatter.format(repo.status[:after_sync], indent: 2)
  end
end
