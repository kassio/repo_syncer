# frozen_string_literal: true

require 'json'
require 'json-schema'

module ReposSyncer
  class Config
    def self.parse = new.tap(&:parse)

    attr_reader :settings, :repositories

    def initialize
      @settings = { colors: true }
      @repositories = {}
    end

    def parse
      repos_files_paths.each do |path|
        next unless File.exist?(path)

        repos_file = File.read(path)
        config = JSON.parse(repos_file, symbolize_names: true)

        validate!(config)
        load_settings(config)
        load_repositories(config)
      end

      self
    end

    private

    def validate!(config)
      validator.validate(config)
    end

    def validator
      @validator ||= JSON::Validator.new(
        File.expand_path('../../schema.json', __dir__)
      )
    end

    def load_settings(config)
      settings[:colors] = config[:settings][:colors] if config[:settings]&.key?(:colors)
      settings[:after_sync] = config[:settings][:afterSync] if config[:settings]&.key?(:afterSync)
    end

    def load_repositories(config)
      repositories.merge!(config.fetch(:repositories, {}))
    end

    def repos_files_paths
      @repos_files_paths ||= [
        ENV.fetch('GSINK_CONFIG_PATH', nil)&.split(':'),
        File.join(File.join(Dir.home, '.config'), 'repos.json'),
        ENV.fetch('XDG_CONFIG_HOME', nil).then do |path|
          File.join(path, 'repos.json') unless path.nil?
        end
      ].flatten.uniq
    end
  end
end
